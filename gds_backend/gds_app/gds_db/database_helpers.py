"""This module provides helper methods to make database interactions reusable.

file:       database_helpers.py
package:    gds_db

"""
from pony.orm import db_session, DatabaseError
from psycopg2.extras import execute_values
from loguru import logger
from pathlib import Path
import csv
from typing import List

from gds_backend.gds_app.gds_db.database_schemas import (
    SCHEMA_NAME,
    GDS_MEASUREMENTS_TABLE,
    GDS_MEASUREMENT_DATA_TABLE,
)
from gds_backend.gds_app.gds_db.database_connection import database


@db_session(strict=True)
def bulk_insert(table_name: str, entity_dicts: List[dict]):
    """Helper function for inserting multiple rows for better performance.

    Args:
        table_name: the table name
        entity_dicts: list of dicitionaries

    Notes:
        Uses keys of first dictionary in list of provided dictionaries to obtain column names.

    """
    tuples: list = [tuple(d.values()) for d in entity_dicts]
    columns: tuple[str, ...] = tuple(str(key) for key in entity_dicts[0].keys())

    column_string: str = str(columns).replace("'", "")

    # Prebuild the sql query
    query = f"Insert into {SCHEMA_NAME}.{table_name}{column_string} values %s"

    try:
        con = database.get_connection()
    except DatabaseError as error:
        logger.error(f"Could not connect to the database - {error}")
        return

    try:
        # Execute the query
        with con.cursor() as cur:
            execute_values(cur, query, tuples)
    except Exception as error:
        logger.error(f"Could not insert new data into the database - {error}")


def import_csv_data(file_path: Path):
    """Helper function for inserting csv data into the database.

    Args:
        file_path: the path to the csv file

    Notes:
        The CSV file should be loaded onto the container/host running the application.

    """
    gds_data = []
    gds_measurement_list = []

    if not file_path.exists():
        logger.error(f"The file does not exist: {file_path}")
        return

    # open file with a with to ensure we close it after reading everything we want
    with open(file_path, newline="\n") as csv_file:
        reader = csv.reader(csv_file)
        header = next(reader, None)  # read the header line

        # header fields could be dynamic
        for index, column_header in enumerate(header):
            if column_header == "measurement":
                measurement_index = index
            elif column_header == "time":
                time_index = index
            elif column_header == "value":
                value_index = index
            elif column_header == "apid":
                apid_index = index

        measurements_found = []

        # Loop through all measurement data
        for measurement_row in reader:

            # Check to see if this measurement was already parsed
            if measurement_row[measurement_index] not in measurements_found:

                # Add new measurement to found list
                measurements_found.append(measurement_row[measurement_index])

                # Create new entry for this measurement
                gds_measurement_dict = {}
                gds_measurement_dict["measurement"] = measurement_row[measurement_index]
                gds_measurement_dict["measurement_description"] = ""

                gds_measurement_list.append(gds_measurement_dict)

            gds_data_dict = {}
            gds_data_dict["measurement"] = measurement_row[measurement_index]
            gds_data_dict["time"] = measurement_row[time_index]
            gds_data_dict["value"] = measurement_row[value_index]
            gds_data_dict["apid"] = measurement_row[apid_index]

            gds_data.append(gds_data_dict)

    # Insert Measurements into Table
    bulk_insert(GDS_MEASUREMENTS_TABLE, gds_measurement_list)

    # Insert Measurement Values into Table
    bulk_insert(GDS_MEASUREMENT_DATA_TABLE, gds_data)

    logger.info(f"Successfully inserted {len(gds_measurement_list)} measurements.")
