"""This module provides the schemas and tables for the database.

file:       database_schemas.py
package:    gds_db

"""
from datetime import datetime

from pony.orm import Database, Optional, PrimaryKey, Required, Set

# constants for schema and table names
SCHEMA_NAME: str = "public"
GDS_MEASUREMENTS_TABLE: str = "measurements"
GDS_MEASUREMENT_DATA_TABLE: str = "measurement_data"


def define_database(db: Database):
    """Define the tables of the database."""

    class GDS_DATA_Measurement(db.Entity):
        """Table for GDS Measurements.

        Attributes:
            measurement (str): the measurement name - primary key
            measurement_data (GDS_Measurement_Values): List of Measurement values with timestamps
            measurement_description (str): the measurement description

        """

        _table_ = (SCHEMA_NAME, GDS_MEASUREMENTS_TABLE)
        measurement = PrimaryKey(str)
        measurement_data = Set(lambda: GDS_Measurement_Values)
        measurement_description = Optional(str)

    class GDS_Measurement_Values(db.Entity):
        """Table for GDS Data.

        Attributes:
            measurement (str): Measurement sensor name
            time (datetime): the time when the value was collected
            value (str): the float value converted to string for memory reduction
            apid (int): random integer associated with the data
        """

        _table_ = (SCHEMA_NAME, GDS_MEASUREMENT_DATA_TABLE)

        measurement = Required(GDS_DATA_Measurement, reverse="measurement_data")
        # Assumption that a measurement can't have multiple values at the same time unless multiple telemetry streams
        time = Required(datetime)
        PrimaryKey(measurement, time)

        # Store float value as a string to reduce the storage
        value = Required(str)

        apid = Required(int)
