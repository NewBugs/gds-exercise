"""This module provides the schemas and tables for the database.

file:       database_schemas.py
package:    gds_db

"""
from pony.orm import Database
from loguru import logger
from pathlib import Path
import yaml

from gds_backend.gds_app.gds_db.database_schemas import define_database


def get_database_configurations() -> dict:
    """Retrieves the database configuration.

    Notes:
        Uses values from the database_configurations.yaml file in the parent directory.

    """
    config_path = (
        Path(__file__).parent.parent.resolve() / "database_configurations.yaml"
    )

    with open(config_path, "r") as config_file:
        configurations = yaml.safe_load(config_file)

    database_configuration = {
        "host": configurations["host"],
        "provider": configurations["provider"],
        "user": configurations["user_var"],
        "password": configurations["password_var"],
        "port": configurations["port_var"],
        "database": configurations["database_var"],
    }

    return database_configuration


def initialize_database_connection(database_configuration: dict) -> Database:
    """Create the pony database object to interact with the database.

    Args:
        database_configuration: the table name

    Notes:
       This will fail if the schemas change or database tables don't match.

    """
    new_db = Database(**database_configuration)

    # associate the database schemas with the database
    define_database(db=new_db)

    new_db.generate_mapping(create_tables=True)

    logger.info("Successfully connected to the PostgreSQL Database.")

    return new_db


# Create global database object to use throughout the application
database = initialize_database_connection(get_database_configurations())
