"""This module provides the API schemas to validate HTTP body requests.

file:       api_schemas.py
package:    gds_api.models

"""
from enum import Enum
from typing import Optional
from datetime import datetime

from pydantic import BaseModel, FilePath


class AvailableDatabases(str, Enum):
    """The possible databases to retrieve or store information to."""

    gds_data = "gds_data"
    gds_user = "gds_user"


class ParseDataModel(BaseModel):
    """Define the fields needed to allow users to parse more data into the database.

    Notes:
        This service only provides interaction to the GDS Data and the GDS Users databases.

    """

    file_path: FilePath
    database: AvailableDatabases


class MeasurementModel(BaseModel):
    """Define the fields needed for a measurement request."""

    measurement: str
    description: Optional[str] = "Default Description"


class MeasurementValuesModel(MeasurementModel):
    """Define the fields needed for a measurement values request."""

    values: list = []


class MeasurementDataModel(MeasurementModel):
    """Define the fields needed for a measurement time associated request."""

    time: datetime
    value: float
    apid: int
