"""This module provides API routes for HTTP requests.

file:       gds_data_routes.py
package:    gds_api

"""
from fastapi import APIRouter, HTTPException
from loguru import logger
from pony.orm import db_session, select, avg

from gds_backend.gds_app.gds_db.database_schemas import (
    GDS_MEASUREMENTS_TABLE,
    GDS_MEASUREMENT_DATA_TABLE,
)
from gds_backend.gds_app.gds_db.database_helpers import import_csv_data, bulk_insert
from gds_backend.gds_app.gds_db.database_connection import database
from gds_backend.gds_app.gds_api.models.api_schemas import (
    ParseDataModel,
    MeasurementDataModel,
)

router = APIRouter(
    prefix="/gds-tools",
    tags=["GDS Tools"],
    responses={404: {"description": "Not Found"}},
)


@router.get("/get-measurements")
async def get_measurements():
    """This route allows users to get all available measurements."""

    try:
        with db_session(strict=True):
            measurement_entry = database.GDS_DATA_Measurement.select().order_by(
                database.GDS_DATA_Measurement.measurement
            )
            measurements = [
                measurement.to_dict(with_collections=True)
                for measurement in measurement_entry
            ]

    except Exception as error:
        message = f"Could not query for measurement: {error}"
        logger.error(message)
        raise HTTPException(status_code=500, detail=message)

    return measurements


@router.get("/get-measurements-values")
async def get_measurement_values():
    """This route allows users to get all available measurement values."""

    try:
        with db_session(strict=True):
            measurement_values = database.GDS_Measurement_Values.select().order_by(
                database.GDS_Measurement_Values.time
            )
            values = [
                value.to_dict(with_collections=True) for value in measurement_values
            ]

    except Exception as error:
        message = f"Could not query the measurement values: {error}"
        logger.error(message)
        raise HTTPException(status_code=500, detail=message)

    return values


@router.post("/parse-data")
async def parse_gds_data(command: ParseDataModel):
    """This route allows users to specify a file path for a csv to be parsed into the database."""

    try:
        import_csv_data(file_path=command.file_path)
        message = f"Successfully parsed CSV data in {command.file_path}"

    # TODO: Replace with specific error handling (ex. Unique error, file missing)
    except Exception as error:
        message = f"Could not import CSV data: {error}"
        logger.error(message)
        raise HTTPException(status_code=500, detail=message)

    return {"Request Response": message}


@router.put("/put-measurement-data")
async def put_measurement_data(command: MeasurementDataModel):
    """This route allows users to add a measurement to the data."""
    gds_data = []
    gds_measurement_list = []

    # Get list of measurments already in database
    measurement_list = await get_measurements()

    # check if measurement already exists
    if not any(
        command.measurement == measurement["measurement"]
        for measurement in measurement_list
    ):
        # Create new entry for this measurement
        gds_measurement_dict = {}
        gds_measurement_dict["measurement"] = command.measurement
        gds_measurement_dict["measurement_description"] = command.description

        gds_measurement_list.append(gds_measurement_dict)

    # Create dict to hold measurement value info
    gds_data_dict = {}
    gds_data_dict["measurement"] = command.measurement
    gds_data_dict["time"] = command.time
    gds_data_dict["value"] = str(command.value)
    gds_data_dict["apid"] = command.apid

    gds_data.append(gds_data_dict)

    try:
        if gds_measurement_list:
            # Insert Measurements into Table
            bulk_insert(GDS_MEASUREMENTS_TABLE, gds_measurement_list)

        if gds_data:
            # Insert Measurement Values into Table
            bulk_insert(GDS_MEASUREMENT_DATA_TABLE, gds_data)

        message = f"Successfully added {command.measurement} and values into database."

    # TODO: Replace with specific error handling (ex. Unique error, file missing)
    except Exception as error:
        message = f"Could not add measurement to the database: {error}"
        logger.error(message)
        raise HTTPException(status_code=500, detail=message)

    return {"Request Response": message}


@router.get("/get-average-measurement-values")
async def get_average_measurement_values():
    """This route allows users to get the average value for each measurement in the database."""
    try:
        with db_session(strict=True):
            # Aggregate query to find the average value for each measurement (convert value to float)
            measurement_entry = list(
                select(
                    (measurement.measurement, avg(float(measurement.value)))
                    for measurement in database.GDS_Measurement_Values
                )
            )

            # Update the first index of the tuple to use the measurement name
            values = [(value[0].measurement, value[1]) for value in measurement_entry]

    except Exception as error:
        message = f"Could not query the measurement values: {error}"
        logger.error(message)
        raise HTTPException(status_code=404, detail=message)

    return values
