"""This module builds and provides the FastAPI backend application.

file:       app_config.py
package:    gds_core

"""
from fastapi import FastAPI

from gds_backend.gds_app.gds_api.gds_data_routes import router


def create_app() -> FastAPI:
    """This method configure and return the FastAPI application for GDS Backend."""
    gds_app = FastAPI(
        title="GDS Exercise",
        version="0.0.1",
        contact={
            "name": "Stephen Faber",
            "email": "faber.stephen@gmail.com",
        },
    )

    # Add the GDS routes to the FastAPI app
    gds_app.include_router(router)

    return gds_app


app = create_app()
