# GDS Exercise

## Backend Packages
API:
- FastAPI
- Uvicorn

DB:
- PonyORM 
- PostgreSQL

## Required Software
- Docker (Linux or Windows Desktop)
- Docker Compose

## Runtime Instructions

1. Clone the repo

```
git clone https://gitlab.com/NewBugs/gds-exercise.git
```

2. (Optional) Add more example CSV files to the following directory:

```
./gds-exercise/gds_backend/example_csv_data/
```

4. Build and Run Docker Compose Service
```
docker-compose run --build
```

4. Launch a web browser and paste the web address to load the SwaggerUI:

[http://127.0.0.1:8000/docs](http://127.0.0.1:8000/docs)


5. Parse CSV Data into the PostgreSQL database using the following REST endpoint (use relative path starting at `gds_backend`):

[http://127.0.0.1:8000/docs#/GDS%20Tools/parse_gds_data_gds_tools_parse_data_post](http://127.0.0.1:8000/docs#/GDS%20Tools/parse_gds_data_gds_tools_parse_data_post)

Example request body (`database` can be left as `gds_data`):

[https://gitlab.com/NewBugs/gds-exercise/-/wikis/web_page_parse_data_screenshot](https://gitlab.com/NewBugs/gds-exercise/-/wikis/web_page_parse_data_screenshot)

6. Use the REST API Endpoints to interact with the data.

[https://gitlab.com/NewBugs/gds-exercise/-/wikis/web_page_screenshot](https://gitlab.com/NewBugs/gds-exercise/-/wikis/web_page_screenshot)
